/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TB2;
// InventoryItem.java
// Represents an item in the inventory.
import java.text.DecimalFormat;

public class InventoryItem {
    private String name;
    private int units;
    private float price;
    private DecimalFormat fmt;

    public InventoryItem(String itemName, int numUnits, float cost) {
        name = itemName;
        units = numUnits;
        price = cost;
        fmt = new DecimalFormat("0.##");
    }

    public String toString() {
        return name + ":\t" + units + " at " + price + " = " + fmt.format((units * price));
    }
}

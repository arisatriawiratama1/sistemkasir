package nilaiuas;

import java.util.*;

/**
 *
 * @author Ari
 */
public class kuis9 {

    public static void main(String[] args) {
        DataTrigo data = new DataTrigo();
        String numberValue;

        Scanner input = new Scanner(System.in);
        System.out.print("Masukan Nilai (minimal 5 angka) : ");
        numberValue = input.nextLine();
        input.close();

        int[] numberValues = Arrays.stream(numberValue.split(" ")).mapToInt(Integer::parseInt).toArray();

        data.sinus(numberValues);
        data.cosinus(numberValues);
        data.tangen(numberValues);
        data.cotangen(numberValues);

    }

}

class DataTrigo {

    public void sinus(int[] values) {
        List sinValues = new ArrayList();
        for (int i = 0; i < values.length; i++) {
            sinValues.add(Math.sin(Math.toRadians(Double.valueOf(values[i]))));
        }

        System.out.println(
                "NILAI SINUS        : " + Arrays.toString(sinValues.toArray()).replace("[", "").replace("]", ""));
    }

    public void cosinus(int[] values) {
        List cosValues = new ArrayList();
        for (int i = 0; i < values.length; i++) {
            cosValues.add(Math.cos(Math.toRadians(Double.valueOf(values[i]))));
        }

        System.out.println(
                "NILAI COSINUS      : " + Arrays.toString(cosValues.toArray()).replace("[", "").replace("]", ""));
    }

    public void tangen(int[] values) {
        List tantValues = new ArrayList();
        for (int i = 0; i < values.length; i++) {
            tantValues.add(Math.tan(Math.toRadians(Double.valueOf(values[i]))));
        }

        System.out.println(
                "NILAI TANGEN       : " + Arrays.toString(tantValues.toArray()).replace("[", "").replace("]", ""));
    }

    public void cotangen(int[] values) {
        List cotValues = new ArrayList();
        for (int i = 0; i < values.length; i++) {
            cotValues.add(1.0 / Math.tan(Math.toRadians(Double.valueOf(values[i]))));
        }

        System.out.println(
                "NILAI COTANGEN     : " + Arrays.toString(cotValues.toArray()).replace("[", "").replace("]", ""));
    }
}

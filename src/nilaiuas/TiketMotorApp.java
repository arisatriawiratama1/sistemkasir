package nilaiuas;
import java.io.*;
import java.util.*;
/**
 *
 * @author Ari
 */
public class TiketMotorApp {
    public static void main(String args[]) throws IOException{
        TiketParkirMotor motor=new TiketParkirMotor("UMB","Ari");
        BufferedReader streami = new BufferedReader(
        new InputStreamReader(System.in));
        System.out.println("Program Input Tiket Parkir Motor");
        System.out.print("No. Plat : ");
        motor.plat=streami.readLine();
        Date jamMasuk=new Date();
        motor.setJamMasuk(jamMasuk.getHours(),jamMasuk.getMinutes(),jamMasuk.getSeconds());
        System.out.print("Tekan Enter Bila Motor Sudah Keluar");
        streami.readLine();
        Date jamKeluar=new Date();
        motor.setJamKeluar(jamKeluar.getHours(),jamKeluar.getMinutes(),jamKeluar.getSeconds());
        System.out.println(motor);
    }
}

package nilaiuas;

import java.util.*;

/**
 *
 * @author Ari
 */
public class forum9 {

    public static void main(String[] args) {

        String nilai;

        Scanner inputFunc = new Scanner(System.in);

        System.out.print("Input angkat (Minimal 5 angka) : ");

        nilai = inputFunc.nextLine();

        inputFunc.close();
        

        int[] nilaiinput = Arrays.stream(nilai.split(" ")).mapToInt(Integer::parseInt).toArray();

        List<Integer> Abs = new ArrayList<Integer>();

        List<Integer> Pangkat = new ArrayList<Integer>();

        List<Float> AkarKuadrat = new ArrayList<Float>();

        for (int i = 0; i < nilaiinput.length; i++) {

            if (nilaiinput[i] == Math.abs(nilaiinput[i])) {

                Abs.add(nilaiinput[i]);

            }

        }

        for (int i = 0; i < nilaiinput.length; i++) {

            Pangkat.add((int) Math.pow(nilaiinput[i], 2));

        }

        for (int i = 0; i < nilaiinput.length; i++) {

            AkarKuadrat.add((float) Math.sqrt(nilaiinput[i]));

        }

        System.out.println("Nilai Maksimum : " + Arrays.stream(nilaiinput).max().getAsInt());

        System.out.println("Nilai Minimum : " + Arrays.stream(nilaiinput).min().getAsInt());

        System.out.println("Nilai Absolut : " + Arrays.toString(Abs.toArray()).replace("[", "").replace("]", ""));

        System.out.println("Nilai Pangkat : " + Arrays.toString(Pangkat.toArray()).replace("[", "").replace("]", ""));

        System.out.println("Nilai Akar Kuadrat : " + Arrays.toString(AkarKuadrat.toArray()).replace("[", "").replace("]", ""));

    }

}

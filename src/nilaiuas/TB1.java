import java.util.Scanner;


class TB1 {

    public static void main(String[] args) {
        Scanner inputFunc = new Scanner(System.in);
        int[] getData = new int[5];

        System.out.print("MASUKKAN BILANGAN PERTAMA : ");
        //Menginput data bilangan pertama
        getData[0] = inputFunc.nextInt();

        System.out.print("MASUKKAN BILANGAN KEDUA : ");
        //Menginput data bilangan kedua
        getData[1] = inputFunc.nextInt();

        System.out.print("MASUKKAN BILANGAN KETIGA : ");
        //Menginput data bilangan ketiga
        getData[2] = inputFunc.nextInt();

        System.out.print("MASUKKAN BILANGAN KEEMPAT : ");
        //Menginput data bilangan keempat
        getData[3] = inputFunc.nextInt();

        System.out.print("MASUKKAN BILANGAN KELIMA : ");
        //Menginput data bilangan kelima
        getData[4] = inputFunc.nextInt();

        int max = getData[0];
        int min = getData[0];
        double mean = 0;

        // max
        for (int i = 0; i < getData.length; i++) {
            if (getData[i] > max) {
                max = getData[i];
            }

            if (getData[i] < min) {
                min = getData[i];
            }

            mean = mean + getData[i];
            if (i == 4) {
                mean = mean / 5;
            }
        }
        System.out.println("BILANGAN TERBESAR ADALAH : " + max);
        //Mengeluarkan bilangan yang terbesar pada inputan 
        System.out.println("BILANGAN TERKECIL ADALAH : " + min);
        //Mengeluarkan bilangan yang terkeccil pada inputan 
        System.out.println("BILANGAN RATA-RATA ADALAH : " + mean);
        //Mengeluarkan bilangan rata-rata terbesar pada inputan 
    }
}
package nilaiuas;

/**
 *
 * @author Ari
 */
public class TiketParkirMotor extends TiketParkir{
    final int HARGA=1000;
    
    public TiketParkirMotor(String loket,String petugas){
        super("Motor");
        this.loket=loket;
        this.petugas=petugas;
    }
    
    public long hitungHarga(){
        if (jumlahJam()==0)
            return (jumlahJam()+1)*HARGA;
        else if ((jumlahMenit()>0) || (jumlahDetik()>0))
            return (jumlahJam()+1)*HARGA;
        else
            return jumlahJam()*HARGA;
    }
    
    public String toString(){
        String data;
        data="\n======================\n"+displayJenis()+"\n======================"+"\nNo. Plat : " + plat +"\nLoket : " + loket + "\nPetugas : " + petugas + "\nJam Masuk :"+jamMasuk+":"+menitMasuk+":"+detikMasuk+"\nJam Keluar :"+jamKeluar+":"+menitKeluar+":"+detikKeluar+"\nLama : " + jumlahJam() + " jam " + jumlahMenit() +" menit "+ jumlahDetik() +" detik "+"\nHarga :" + hitungHarga();
        return data;
    } 
    
    public int jumlahJam(){
        int jam=0;
        if (menitKeluar>=menitMasuk)
            jam=jamKeluar-jamMasuk;
        else
            jam=jamKeluar-jamMasuk-1;
        return jam;
    }
   
    public int jumlahMenit(){
        int menit=0;
        if (menitKeluar>=menitMasuk)
            menit = menitKeluar-menitMasuk;
        else
            menit=menitMasuk-menitKeluar;
        
        if (!(detikKeluar>=detikMasuk))
        menit=menit-1;
        return menit;
    }

    public int jumlahDetik(){
        int detik=0;
        if (detikKeluar>=detikMasuk)
        detik = detikKeluar-detikMasuk;
    else
        {
            if (detikMasuk==0)
            detik=60-detikKeluar;
    else
            detik=detikMasuk-detikKeluar;
        } 
        return detik;
    }

 public void setJamMasuk(int jam,int menit,int detik){
     this.jamMasuk=jam;
     this.menitMasuk=menit;
     this.detikMasuk=detik;
    }

 public void setJamKeluar(int jam,int menit,int detik){ 
     this.jamKeluar=jam;
     this.menitKeluar=menit;
     this.detikKeluar=detik;
    } 
}
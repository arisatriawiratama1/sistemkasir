import java.util.Scanner;

public class Piramidangka {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int n,i,j,k = 1;
		System.out.print("Masukkan nilai angka = ");
		n = scanner.nextInt();
		for(i=1; i<=n; i++) {
			k = 0;
			while(k != (n-i)) {
				System.out.print(" ");
				k++;
			}
			for(j=1; j<=(2*i)-1;j++) {
				if(j==1||j==((2*i)-1)){
					System.out.print(i);
				} else {
					System.out.print("0");
				}
			}
			System.out.println();
		}
	}
}
package Nilaiuas;
import java.util.Scanner;
public class nilaiuas {
public static void main(String[] args) {
    /**
 *
 * @author Ari Satria Wiratama
 */
        Scanner input = new Scanner (System.in);
        double nilaitb1, nilaitb2, nilaiuas, nilaihasil;
        System.out.println("                                DAFTAR NILAI SEMESTER GENAP TAHUN AKADEMIK 2020/2021");
        System.out.println("                                                UNIVERSITAS MERCU BUANA             ");
        System.out.println("                                                 FAKULTAS ILMU KOMPUTER             ");
        System.out.println("                                         PROGRAM STUDI : TEKNIK INFORMATIKA         ");
        System.out.println(" ");
        System.out.print("Masukkan Nilai Tugas Besar 1 : ");
        nilaitb1 = input.nextDouble(); 
        System.out.print("Masukkan Nilai Tugas Besar 2 : ");
        nilaitb2 = input.nextDouble(); 
        System.out.print("Masukkan Nilai UAS           : ");
        nilaiuas = input.nextDouble(); 
        nilaihasil = (nilaitb1 * 0.3) + (nilaitb2 * 0.3) + (nilaiuas * 0.4);
        System.out.println("Nilai Akhir               : " +nilaihasil);
    }    
}

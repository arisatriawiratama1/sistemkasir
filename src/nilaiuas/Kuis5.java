package Kuis5;

import java.util.Scanner;


public class Kuis5 {

    public static void main (String args[]){

        Scanner scan = new Scanner (System.in);

        int nilaiA, nilaiB;
        boolean pro;

        System.out.print("Masukkan Nilai A : ");
        nilaiA = scan.nextInt(); 
        

        System.out.print("Masukkan Nilai B : ");
        nilaiB = scan.nextInt();


        System.out.println(" ");
        System.out.println(" ");
        pro = (nilaiA>nilaiB);
        System.out.println("1. Apakah  "+nilaiA+" > "+nilaiB+" ) : "+pro);
        
        pro = (nilaiA<=nilaiB);
        System.out.println("2. Apakah  "+nilaiA+" <= "+nilaiB+" ) : "+pro);
        
        pro = (nilaiA==nilaiB);
        System.out.println("3. Apakah  "+nilaiA+" == "+nilaiB+" ) : "+pro);
        
        pro = (nilaiA!=nilaiB);
        System.out.println("4. Apakah  "+nilaiA+" != "+nilaiB+" ) : "+pro);
        
        pro = (nilaiA>=nilaiB);
        System.out.println("5. Apakah  "+nilaiA+" >= "+nilaiB+" ) : "+pro);  
    }
}
package nilaiuas;
import java.util.Scanner;
import java.util.ArrayList;
/**
 *
 * @author Ari
 */
public class Kuis7 {
    public static void main(String[] args) throws Exception {
        Scanner inputFunc = new Scanner(System.in);
        Data getKaryawan = new Data();
        int divisi;

        System.out.println("Pilih Divisi");
        System.out.println("1. Divisi IT");
        System.out.println("2. Divisi SDM");
        System.out.println("\n");
        System.out.print("Masukkan Nomor Divisi : ");
        divisi = inputFunc.nextInt();
        inputFunc.close();

        getKaryawan.DataNIK(divisi);

    }
}

class Data {

    String[] title = { "NIK", "NAMA", "KELAMIN", "EMAIL", "ALAMAT" };
    String[][] divisiIT = { { "1005009500", "Ari Satria Wiratama", "Laki-laki", "mamang@mail.com", "jl.Haji gedad" },
            { "1005009700", "jarwo", "Laki-laki", "farah@mail.com", "jl.km hasyari" } };
    String[][] divisiSDM = { { "1005009800", "Jonny", "Laki-laki", "yayat@mail.com", "jl.koplo gaming" },
            { "1005009600", "malih", "Laki-laki", "malih@mail.com", "jl.kimochih" } };

    public void DataNIK(int division) {
        switch (division) {
            case 1:
                for (int i = 0; i < divisiIT.length; i++) {
                    System.out.println("\n------ Data karyawan -------");
                    for (int j = 0; j < divisiIT[i].length; j++) {
                        System.out.print(title[j] + " : " + divisiIT[i][j] + " ");
                        System.out.println("");
                    }
                }
                break;

            case 2:
                for (int i = 0; i < divisiSDM.length; i++) {
                    System.out.println("\n------ Data karyawan -------");
                    for (int j = 0; j < divisiSDM[i].length; j++) {
                        System.out.print(title[j] + " : " + divisiSDM[i][j] + " ");
                        System.out.println("");
                    }
                }
                break;

            default:
                System.out.println("Divisi yang dicari tidak ditemukan!");
                break;
        }
    }
}
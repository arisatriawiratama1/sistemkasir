/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nilaiuas;

/**
 *
 * @author Ari
 */
class FirstThread extends Thread{
    public void run(){
        for (int i=1; i<=10; i++){
            System.out.println( "Messag from First Thread  : " +i);
            try{
                  Thread.sleep(1000);
            }catch (InterruptedException interruptedException){
             System.err.println( "First Thread is interrupted when it is sleeping"+interruptedException);
            }
        }
    }
} 

class SecondThread extends Thread{
 //This method will be executed when this thread is executed
    public void run(){
        for (int i=1; i<=10; i++){
            System.out.println( "Messag from Second Thread : " +i);
            try{
                  Thread.sleep (1000);
            }catch (InterruptedException interruptedException){
             System.err.println( "second Thread is interrupted when it is sleeping"+interruptedException);
            }
        }
    }
}
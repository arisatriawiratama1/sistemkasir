package nilaiuas;
        
        
class Hewan {

    String NamaHewan, TipeHewan, TmptTinggal,keterangan;

    public void tampil() {

        System.out.println("\n====================================");

        System.out.println("Nama Hewan              :" + NamaHewan);

        System.out.println("Tipe Hewan              :" + TipeHewan);

        System.out.println("Habitat                 :" +TmptTinggal);
        
        System.out.println("keterangan              :" +keterangan);

  }



}


class Kucing extends Hewan {

    public Kucing (String nama, String jenis, String habitat,String keterangan){

     this.NamaHewan    = nama;

     this.TipeHewan   = jenis;

     this.TmptTinggal = habitat;
     
     this.keterangan = keterangan;

    }

}



class Kantor2 extends Hewan {

    public Kantor2 (String nama, String jenis, String habitat,String keterangan){

     this.NamaHewan    = nama;

     this.TipeHewan   = jenis;

     this.TmptTinggal = habitat;
     
     this.keterangan = keterangan;

    }

}



class IkanBelut extends Hewan {

    public IkanBelut (String nama, String jenis, String habitat,String keterangan){

     this.NamaHewan    = nama;

     this.TipeHewan   = jenis;

     this.TmptTinggal = habitat;

     this.keterangan = keterangan;
    }

}



class Gurita extends Hewan {

    public Gurita (String nama, String jenis, String habitat,String keterangan){

     this.NamaHewan    = nama;

     this.TipeHewan   = jenis;

     this.TmptTinggal = habitat;
     
     this.keterangan = keterangan;

    }

}



class Lebah extends Hewan {

    public Lebah (String nama, String jenis, String habitat,String keterangan){

     this.NamaHewan    = nama;

     this.TipeHewan   = jenis;

     this.TmptTinggal = habitat;
     
     this.keterangan = keterangan;

    }

}



public class forum8 {

    public static void main(String[] args) {

        System.out.println("Klasifikasi Hewan");

        Kucing kucing = new Kucing("Kucing", "Darat", "Dirumah & Dijalan","Hewan Karnivora");

        Kantor2 anjing = new Kantor2("Anjing", "Darat", "Dirumah & Dijalan","Hewan Karnivora");

        IkanBelut ikanbelut = new IkanBelut("IkanBelut", "Air", "Lautan","Hewan Karnivora");

        Gurita gurita = new Gurita("Gurita", "Air", "Lautan","Hewan Karnivora");

        Lebah lebah = new Lebah("Lebah", "Darat", "Hutan","Hewan Herbivora");



        kucing.tampil();

        anjing.tampil();

        ikanbelut.tampil();

        gurita.tampil();

        lebah.tampil(); 

    }

}
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aplikasikasir;
import java.text.NumberFormat;
import java.util.StringTokenizer;
import java.util.Locale;
/**
 *
 * @author Ari
 */
public class Frameaplikasi extends javax.swing.JFrame {
    String nm_mknan;
    int harga_mknan;
    int jml_beli;
    int jumlah_hrg;
    int kembalian;
    int jumlah_byr;
    int bayar,beli,Harga,bungkus,pph,kembali;
    /**
     * Creates new form Frameaplikasi
     */
    public Frameaplikasi() {
        initComponents();
    }

    public void radio(){
    if(rdbungkus.isSelected()){
    rddisini.setSelected(false);
    bungkus = bayar + 2000 ;
    TmtjmlBayar.setText(""+bungkus);
    }if(rddisini.isSelected()){
    rdbungkus.setSelected(false);
    TmtjmlBayar.setText(""+bayar);
        }
    }
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        CBNNamaMkn = new javax.swing.JComboBox<>();
        txtharga = new javax.swing.JTextField();
        txtbeli = new javax.swing.JTextField();
        ButtonHitungJuml = new javax.swing.JButton();
        TmtjmlBayar = new javax.swing.JTextField();
        rdbungkus = new javax.swing.JRadioButton();
        rddisini = new javax.swing.JRadioButton();
        txtBayar = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        jButton2 = new javax.swing.JButton();
        TxtKembalian = new javax.swing.JTextField();
        jbonus = new javax.swing.JCheckBox();
        jButton3 = new javax.swing.JButton();
        Reset = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel1.setText("Nama Makanan");
        getContentPane().add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(12, 75, -1, -1));

        jLabel2.setText("Harga Makanan");
        getContentPane().add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(12, 115, -1, -1));

        jLabel3.setText("Jumlah Beli");
        getContentPane().add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(12, 155, -1, -1));

        jLabel4.setText("Jumlah Harga");
        getContentPane().add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(420, 70, -1, -1));

        jLabel5.setText("Jumlah Bayar");
        getContentPane().add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(420, 130, -1, -1));

        jLabel6.setText("Jumlah Kembalian");
        getContentPane().add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(420, 190, -1, -1));

        CBNNamaMkn.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Pilih Menu", "Nasi Goreng", "Babi Guling", "Mie Ayam", "Kwetiaw" }));
        CBNNamaMkn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CBNNamaMknActionPerformed(evt);
            }
        });
        getContentPane().add(CBNNamaMkn, new org.netbeans.lib.awtextra.AbsoluteConstraints(145, 72, -1, -1));

        txtharga.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txthargaActionPerformed(evt);
            }
        });
        getContentPane().add(txtharga, new org.netbeans.lib.awtextra.AbsoluteConstraints(145, 112, 173, -1));
        getContentPane().add(txtbeli, new org.netbeans.lib.awtextra.AbsoluteConstraints(145, 152, 173, -1));

        ButtonHitungJuml.setText("Hitung Jumlah Bayar");
        ButtonHitungJuml.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonHitungJumlActionPerformed(evt);
            }
        });
        getContentPane().add(ButtonHitungJuml, new org.netbeans.lib.awtextra.AbsoluteConstraints(150, 190, -1, -1));
        getContentPane().add(TmtjmlBayar, new org.netbeans.lib.awtextra.AbsoluteConstraints(540, 70, 175, -1));

        rdbungkus.setText("Bungkus");
        rdbungkus.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rdbungkusActionPerformed(evt);
            }
        });
        getContentPane().add(rdbungkus, new org.netbeans.lib.awtextra.AbsoluteConstraints(540, 100, -1, -1));

        rddisini.setText("Makan Disini");
        rddisini.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rddisiniActionPerformed(evt);
            }
        });
        getContentPane().add(rddisini, new org.netbeans.lib.awtextra.AbsoluteConstraints(620, 100, -1, -1));

        txtBayar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtBayarActionPerformed(evt);
            }
        });
        getContentPane().add(txtBayar, new org.netbeans.lib.awtextra.AbsoluteConstraints(540, 130, 175, -1));

        jLabel7.setFont(new java.awt.Font("Tahoma", 1, 20)); // NOI18N
        jLabel7.setText("KASIR RESTORAN KELOMPOK 6");
        getContentPane().add(jLabel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(260, 10, -1, -1));

        jButton2.setText("Bayar");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });
        getContentPane().add(jButton2, new org.netbeans.lib.awtextra.AbsoluteConstraints(740, 130, -1, -1));
        getContentPane().add(TxtKembalian, new org.netbeans.lib.awtextra.AbsoluteConstraints(540, 190, 178, -1));

        jbonus.setText("pph 5%");
        jbonus.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbonusActionPerformed(evt);
            }
        });
        getContentPane().add(jbonus, new org.netbeans.lib.awtextra.AbsoluteConstraints(540, 220, -1, -1));

        jButton3.setText("Close");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });
        getContentPane().add(jButton3, new org.netbeans.lib.awtextra.AbsoluteConstraints(820, 0, -1, -1));

        Reset.setText("Reset");
        Reset.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ResetActionPerformed(evt);
            }
        });
        getContentPane().add(Reset, new org.netbeans.lib.awtextra.AbsoluteConstraints(380, 270, -1, -1));

        setSize(new java.awt.Dimension(896, 377));
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void CBNNamaMknActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_CBNNamaMknActionPerformed
        // TODO add your handling code here:
    nm_mknan=(String) CBNNamaMkn.getSelectedItem();
    if (nm_mknan=="Nasi Goreng")
    {
    harga_mknan=25000;
    }else if (nm_mknan=="Babi Guling")
    {
    harga_mknan=150000;
    }else if (nm_mknan=="Mie Ayam")
    {     
    harga_mknan=15000;
    }else if (nm_mknan=="Kwetiaw")
    {  
    harga_mknan=20000;
    }
//gantiformat=NumberFormat.getNumberInstance(Locale.ENGLISH).format(harga_mknan);
//token=new StringTokenizer(gantiformat,”.”);
//gantiformat=token.nextToken();
//gantiformat=gantiformat.replace(‘.’,’.’ );
    txtharga.setText(""+harga_mknan);
    }//GEN-LAST:event_CBNNamaMknActionPerformed

    private void txthargaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txthargaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txthargaActionPerformed

    private void rdbungkusActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rdbungkusActionPerformed
        // TODO add your handling code here:
        radio();
    }//GEN-LAST:event_rdbungkusActionPerformed

    private void rddisiniActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rddisiniActionPerformed
        // TODO add your handling code here:
        radio ();
    }//GEN-LAST:event_rddisiniActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        // TODO add your handling code here:
        this.dispose();
    }//GEN-LAST:event_jButton3ActionPerformed

    private void txtBayarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtBayarActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtBayarActionPerformed

    private void ButtonHitungJumlActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonHitungJumlActionPerformed
        // TODO add your handling code here:
        //buttonGroup1.clearSelection();
        txtBayar.setText("");
        //txtHarga.setText("");
        TxtKembalian.setText("");
        Harga =Integer.parseInt(txtharga.getText());
        beli = Integer.parseInt(txtbeli.getText ());
        
        bayar = Harga * beli ;
        TmtjmlBayar.setText(""+bayar);
    }//GEN-LAST:event_ButtonHitungJumlActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        // TODO add your handling code here:
        int duit = Integer.parseInt(txtBayar.getText());
        kembali = duit - bayar;
        if(rdbungkus.isSelected()){
            kembali = kembali - 2000;
        }
        TxtKembalian.setText(""+kembali);
    }//GEN-LAST:event_jButton2ActionPerformed

    private void jbonusActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbonusActionPerformed
        // TODO add your handling code here:
        if(jbonus.isSelected()){
            pph = kembali*5/100;
            kembali= kembali-pph;
            
        }
        TxtKembalian.setText(""+kembali);
    }//GEN-LAST:event_jbonusActionPerformed

    private void ResetActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ResetActionPerformed
        // TODO add your handling code here:
        CBNNamaMkn.setSelectedIndex(0);
        txtharga.setText("");
        txtbeli.setText("");
        TmtjmlBayar.setText("");
        txtBayar.setText("");
        TxtKembalian.setText("");
        jbonus.setSelected(false);
        rdbungkus.setSelected(false);
        rddisini.setSelected(false);
    }//GEN-LAST:event_ResetActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Frameaplikasi.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Frameaplikasi.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Frameaplikasi.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Frameaplikasi.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Frameaplikasi().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton ButtonHitungJuml;
    private javax.swing.JComboBox<String> CBNNamaMkn;
    private javax.swing.JButton Reset;
    private javax.swing.JTextField TmtjmlBayar;
    private javax.swing.JTextField TxtKembalian;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JCheckBox jbonus;
    private javax.swing.JRadioButton rdbungkus;
    private javax.swing.JRadioButton rddisini;
    private javax.swing.JTextField txtBayar;
    private javax.swing.JTextField txtbeli;
    private javax.swing.JTextField txtharga;
    // End of variables declaration//GEN-END:variables
}
